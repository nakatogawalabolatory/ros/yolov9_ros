#!/usr/bin/env python3

# YOLO
from ultralytics import YOLO

# general
import numpy as np
np.float = np.float64  # rps_numpy の float 設定を変更する
import cv2

# custom msg
from yolov9_ros_msgs.msg import Yolov9Object, Yolov9ObjectArray
# ros pkgs
from sensor_msgs.msg import PointCloud2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import ros2_numpy as ros_numpy
from rclpy.node import Node
import rclpy

class Yolov9Ros:
    def __init__(self, node, model=None):
        self.node = node
        self.bridge = CvBridge()
        if model is None:
            self.node.get_logger().warn('Download the model because no model data is specified.')
            #self.model = YOLO('yolov9c.pt') # データが指定されていない場合はダウンロードされる
            self.model = YOLO('yolov9c-seg.pt') # データが指定されていない場合はダウンロードされる
        else:
            self.model = YOLO()
            self.model.load_weights(model)
        self.depth_src = self.node.create_subscription(PointCloud2, '/camera/depth_registered/points', self.depth_cb, 10)
        self.img_src = self.node.create_subscription(Image, '/image_raw', self.image_cb, 10) # Xtion からのとピクを取得
        self.img_pub = self.node.create_publisher(Image, 'yolopv9_ros/detect_image', 10)
        self.object_pub = self.node.create_publisher(Yolov9ObjectArray, 'yolopv9_ros/object_array', 10)
        self.node.get_logger().info('YoloV9_ROS Active !')
        try:
            rclpy.spin(self.node)
        except KeyboardInterrupt:
            self.node.destroy_node()

    def image_cb(self, data):
        try:
            color = self.bridge.imgmsg_to_cv2(data,"rgb8") 
        except CvBridgeError as e :
            self.node.get_logger().error(e)
        color = cv2.cvtColor(color, cv2.COLOR_BGR2RGB)
        results = self.model.predict(source=color, conf=0.5, verbose=False)
        result_img = results[0].plot()

        ###
        yolo_object_array = Yolov9ObjectArray()
        classes_map = results[0].names
        classes = results[0].boxes.cls
        bboxes = results[0].boxes.xyxy
        confs = results[0].boxes.conf
        try:
            masks = results[0].masks.data
            segment = True
        except AttributeError:
            segment = False
            #self.node.get_logger().warn('not segment')
            masks = results[0].boxes.cls
        i = 0
        for bbox, cls, conf, mask in zip(bboxes, classes, confs, masks):
            yolo_object = Yolov9Object()
            x1, y1, x2, y2 = map(int, bbox)
            if segment:
                segment_image = (mask.cpu().numpy() * 255).astype('uint8')
                yolo_object.segment_image = self.bridge.cv2_to_imgmsg(cv2.cvtColor(segment_image, cv2.COLOR_GRAY2BGR), encoding="passthrough")
                yolo_object.segment_image.header = data.header
            yolo_object.conf = conf.item()
            yolo_object.class_number = int(cls)
            yolo_object.class_name = '%s_%d'%(classes_map.get(int(cls)), i)
            yolo_object.min_x = x1
            yolo_object.min_y = y1
            yolo_object.max_x = x2
            yolo_object.max_y = y2
            yolo_object_array.array.append(yolo_object)
            i += 1
        if i > 0:
            yolo_object_array.header = data.header
            self.object_pub.publish(yolo_object_array)
        ###
        #if segment:
        #    cv2.imshow('debug', segment_image)
        cv2.imshow('YOLOv9', result_img)
        if cv2.waitKey(1) & 0xFF == 27:
            cv2.destroyAllWindows()
            self.node.destroy_node()
        result_img_msg = self.bridge.cv2_to_imgmsg(result_img, encoding="bgr8")
        result_img_msg.header = data.header
        self.img_pub.publish(result_img_msg)

    def depth_cb(self, data):
        self.node.get_logger().info('YoloV9_ROS Active !')
        point_data = ros2_numpy.numpify(data)
        image_data = point_data['rgb'].view((np.uint8, 4))[..., [2, 1, 0]]
        image_rgb = cv2.cvtColor(image_data, cv2.COLOR_BGR2RGB)
        
        results = self.model(image_rgb)  # YOLO inference
        result_image = results.render()
        
        cv2.imshow('debug', result_image)
        if cv2.waitKey(1) & 0xFF == 27:
            cv2.destroyAllWindows()
            rclpy.shutdown(self.node)

if __name__ == '__main__':
    rclpy.init()
    node = Node('yolov9_ros')
    yolov9_ros = Yolov9Ros(node)
