#!/usr/bin/env python3
from yolov9_ros_msgs.msg import Yolov9Pose, Yolov9PoseArray
from sensor_msgs.msg import PointCloud2
from sensor_msgs_py import point_cloud2 as pc2
from cv_bridge import CvBridge, CvBridgeError
from rclpy.node import Node
from rclpy.qos import QoSProfile, QoSReliabilityPolicy
import rclpy
import ros2_numpy

import numpy as np
import cv2

class HumanDetector:
    def __init__(self, node):
        self.node = node
        qos_profile = QoSProfile(depth=10, reliability=QoSReliabilityPolicy.BEST_EFFORT)
        self.node.create_subscription(PointCloud2, 'topic_name', self.pose_detection_cb, qos_profile)
        self.node.get_logger().info('HumanDetector Active !')
        try:
            rclpy.spin(self.node)
        except KeyboardInterrupt:
            self.node.destroy_node()

    def pose_detection_cb(self, data):
        # ポイントクラウドデータをリストに変換
        point_cloud = list(pc2.read_points(data, field_names=("x", "y", "z", "rgb"), skip_nans=True))
        
        # RGB データを抽出して整数型に変換
        rgb_data = np.array([point[3] for point in point_cloud], dtype=np.uint32)
        
        # RGB データをバイトに分割して NumPy 配列に変換
        r = ((rgb_data >> 16) & 0xff).astype(np.uint8)
        g = ((rgb_data >> 8) & 0xff).astype(np.uint8)
        b = (rgb_data & 0xff).astype(np.uint8)
        
        # RGB データを 3 次元の NumPy 配列に変換して結合
        rgb_data_np = np.stack((r, g, b), axis=-1)
        
        # イメージデータに変換
        image_data = cv2.cvtColor(rgb_data_np, cv2.COLOR_RGB2BGR)
        
        # 画像サイズをリサイズ
        image_data = cv2.resize(image_data, (640, 480))
        print(image_data.shape)

        print(type(image_data))
        print(image_data)

        # show
        cv2.imshow('HUMAN_DETECTION', image_data)
        if cv2.waitKey(1) & 0xFF == 27:
            cv2.destroyAllWindows()
            self.node.destroy_node()

if __name__ == '__main__':
    rclpy.init()
    node = Node('human_detector')
    human_detector = HumanDetector(node)
