#!/usr/bin/env python3

# YOLO
from ultralytics import YOLO

# general
import numpy as np
np.float = np.float64  # rps_numpy の float 設定を変更する
import cv2

# custom msg
from yolov9_ros_msgs.msg import Yolov9Pose, Yolov9PoseArray
# ros pkgs
#from sensor_msgs.msg import PointCloud2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import ros2_numpy as ros_numpy
from rclpy.node import Node
import rclpy

class Yolov9RosPose:
    def __init__(self, node, model=None):
        self.node = node
        self.bridge = CvBridge()
        if model is None:
            self.node.get_logger().warn('Download the model because no model data is specified.')
            #self.model = YOLO('yolov9c.pt') # データが指定されていない場合はダウンロードされる
            self.model = YOLO('yolov8n-pose.pt') # データが指定されていない場合はダウンロードされる
        else:
            self.model = YOLO()
            self.model.load_weights(model)
        #self.src = self.node.create_subscription(PointCloud2, '/camera/depth_registered/points', self.depth_cb, 10)
        self.src = self.node.create_subscription(Image, '/image_raw', self.image_cb, 10) # Xtion からのとピクを取得
        self.img_pub = self.node.create_publisher(Image, 'yolopv9_ros/detect_pose_image', 10)
        self.pose_pub = self.node.create_publisher(Yolov9PoseArray, 'yolopv9_ros/pose_array', 10)
        self.node.get_logger().info('YoloV9_ROS_POSE Active !')
        try:
            rclpy.spin(self.node)
        except KeyboardInterrupt:
            self.node.destroy_node()

    def image_cb(self, data):
        try:
            color = self.bridge.imgmsg_to_cv2(data,"rgb8") 
        except CvBridgeError as e :
            self.node.get_logger().error(e)
        color = cv2.cvtColor(color, cv2.COLOR_BGR2RGB)
        results = self.model.predict(source=color, conf=0.5, verbose=False)
        result_img = results[0].plot()

        yolo_pose_array = Yolov9PoseArray()
        names = results[0].names
        classes = results[0].boxes.cls
        boxes = results[0].boxes
        confs = results[0].boxes.conf
        keypoints = results[0].keypoints

        if keypoints.has_visible:
            for box, cls, conf, keypoints, keyconfs in zip(boxes, classes, confs, keypoints.xy, keypoints.conf):
                yolo_pose = Yolov9Pose()
                x1, y1, x2, y2 = [int(i) for i in box.xyxy[0]]
                keypoint = keypoints.tolist()
                keyconf = keyconfs.tolist()
                yolo_pose.conf = conf.item()
                yolo_pose.class_number = int(cls)
                yolo_pose.min_x = x1
                yolo_pose.min_y = y1
                yolo_pose.max_x = x2
                yolo_pose.max_y = y2

                yolo_pose.nose = [keypoint[0][0], keypoint[0][1], keyconf[0]]
                yolo_pose.left_eye = [keypoint[1][0], keypoint[1][1], keyconf[1]]
                yolo_pose.right_eye = [keypoint[2][0], keypoint[2][1], keyconf[2]]
                yolo_pose.left_ear = [keypoint[3][0], keypoint[3][1], keyconf[3]]
                yolo_pose.right_ear = [keypoint[4][0], keypoint[4][1], keyconf[4]]
                yolo_pose.left_shoulder = [keypoint[5][0], keypoint[5][1], keyconf[5]]
                yolo_pose.right_shoulder = [keypoint[6][0], keypoint[6][1], keyconf[6]]
                yolo_pose.left_elbow = [keypoint[7][0], keypoint[7][1], keyconf[7]]
                yolo_pose.right_elbow = [keypoint[8][0], keypoint[8][1], keyconf[8]]
                yolo_pose.left_wrist = [keypoint[9][0], keypoint[9][1], keyconf[9]]
                yolo_pose.right_wrist = [keypoint[10][0], keypoint[10][1], keyconf[10]]
                yolo_pose.left_waist = [keypoint[11][0], keypoint[11][1], keyconf[11]]
                yolo_pose.right_waist = [keypoint[12][0], keypoint[12][1], keyconf[12]]
                yolo_pose.left_knee = [keypoint[13][0], keypoint[13][1], keyconf[13]]
                yolo_pose.right_knee = [keypoint[14][0], keypoint[14][1], keyconf[14]]
                yolo_pose.left_ankle = [keypoint[15][0], keypoint[15][1], keyconf[15]]
                yolo_pose.right_ankle = [keypoint[16][0], keypoint[16][1], keyconf[16]]

                #print(yolo_pose)
                yolo_pose_array.array.append(yolo_pose)
            yolo_pose_array.header = data.header
            self.pose_pub.publish(yolo_pose_array)

        cv2.imshow('YOLOv9', result_img)
        if cv2.waitKey(1) & 0xFF == 27:
            cv2.destroyAllWindows()
            self.node.destroy_node()
        result_img_msg = self.bridge.cv2_to_imgmsg(result_img, encoding="bgr8")
        result_img_msg.header = data.header
        self.img_pub.publish(result_img_msg)

if __name__ == '__main__':
    rclpy.init()
    node = Node('yolov9_ros_pose')
    yolov9_ros = Yolov9RosPose(node)
