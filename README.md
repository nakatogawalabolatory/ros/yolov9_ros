# yolov9_ros Humble
　**[YOLO_v9](https://github.com/WongKinYiu/yolov9)** を ROS2 humble で使用するための Docker コンテナを提供します。

# インストール
　インストール先の PC に以下の環境が整っていることを確認してください。

- **Docker**
- **CUDA**
- **ROS2 humble**

　ROS ワークスペース内にこのリポジトリをダウンロードします。
```bash
git clone -b $ROS_DISTRO https://gitlab.com/nakatogawalabolatory/ros/yolov9_ros.git
```

# ビルド

> **NVIDIA GPU 非搭載の PC で本パッケージを使用する場合**<br>
    すべての docker コマンドにおける `yolov9_ros2` を **`yolov9_ros2-cpu`** に書き換えてください。

　コンテナをビルドするには以下のコマンドを実行してください。
```bash
docker compose build yolov9_ros2
```
　コンテナを常に最新の状態にしつつすぐにコンテナを稼働させたい場合は以下のコマンドを実行してください。

　バックグラウンドでコンテナを起動させたい場合は上記コマンドにオプション `-d` を追加します。
```bash
docker compose up -d --build yolov9_ros2
```

　ホスト側でもこのリポジトリにあるパッケージをビルドする必要があるのでこのリポジトリがあるワークスペースで以下のコマンドを実行してください。
```bash
colcon build
```
　もしこのリポジトリのみビルドしたい場合は以下のコマンドを実行してください。
```bash
colcon build --packages-up-to yolov9_ros
```

# つかいかた
> 現在作成中…
    　クイックスタートの部分だけ参照してください。

## クイックスタート
　ホスト側でカメラデータをパブリッシュするプログラムを実行してください。例えば
```bash
ros2 run usb_cam usb_cam_node
# or
ros2 launch openni2_camera camera_with_cloud.launch.py 
```
などです。<br>
　次に本パッケージ下の `docker-compose.yaml` の 37 行目の `command` を編集します。デフォルトでは以下のように書かれています。

> CPU 環境の方は **66行目の `command`** を編集してください。

```yaml
command: /bin/bash -c "source /colcon_ws/install/setup.bash; ros2 run yolov9_ros yolov9_ros.py --ros-args --remap /image_raw:=/TOPIC_NAME
```
　この行の一番右側の `/TOPIC_NAME` に検出させたいトピック名に書き換えてください。例えば、Xtion カメラから推論させたい場合は以下のように書き換えます。
```yaml
command: /bin/bash -c "source /colcon_ws/install/setup.bash; ros2 run yolov9_ros yolov9_ros.py --ros-args --remap /image_raw:=/camera/rgb/image_raw
```
　編集をしたら以下のコマンドを実行してコンテナをビルド、実行します。
```bash
docker compose up --build yolov9_ros2
```
　すると推論結果が表示されます。そして `/yolopv9_ros/detect_image` という検出結果イメージを格納したトピックがパブリッシュされます。パブリッシュされるデータは以下のとおりです。

- **/yolopv9_ros/object_array**<br>
    |**データ名**|**内容**|
    |:---:|:---|
    |$\text{min\_x}$|`int16` 検出したオブジェクトのバウンディングボックス左上頂点座標 `x`|
    |$\text{min\_y}$|`int16` 検出したオブジェクトのバウンディングボックス左上頂点座標 `y`|
    |$\text{max\_x}$|`int16` 検出したオブジェクトのバウンディングボックス右下頂点座標 `x`|
    |$\text{max\_y}$|`int16` 検出したオブジェクトのバウンディングボックス右下頂点座標 `y`|
    |$\text{conf}$|`float32` 検出したオブジェクトの検出確率|
    |$\text{class\_number}$|`int16` 検出したオブジェクトのクラス番号|
    |$\text{class\_name}$|`string` 検出したオブジェクト名|
    |$\text{segment\_image}$|`Image` 検出したオブジェクトのセグメントマスク画像。<br>　セグメント推論以外の場合ここにはデータは入らない。|
    
- **/yolopv9_ros/detect_image**<br>
    推論状況（バウンディングボックスの描画など）がプロットされた検出画像。

## トレーニング

# デバッグ
　コンテナが起動中に以下のコマンドを実行するとコンテナ内にアクセスできます。
```bash
docker compose exec yolov9_ros2 /bin/bash
```
