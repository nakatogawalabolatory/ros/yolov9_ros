#!/bin/bash
set -e

echo "setup finish"
source /opt/ros/humble/setup.bash
source /colcon_ws/install/setup.bash
source ~/.bashrc
exec "$@"
